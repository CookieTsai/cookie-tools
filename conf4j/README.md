## Conf4j

這是一個非常小的專案, 可以建立一個簡單的規則, 幫助你去維護及設定系統所需的參數

**目錄結構**

```print
Maven_Project
|____conf
| |____test-defined.conf // 自定義變數
|____src
| |____main
|   |____java
|   |____resources
|     |____test-default.conf // 預設變數
|____pom.xml
|____README.md
```

### 1. 安裝

```bash
$ git clone https://gitlab.com/CookieTsai/cookie-tools.git
$ mvn -f cookie-tools/pom.xml clean install
```

#### 2. 設定 `pom.xml`

```xml
<!-- conf4j -->
<dependency>
    <groupId>tw.cookie.tools</groupId>
    <artifactId>conf4j</artifactId>
    <version>0.1.0</version>
</dependency>
```

#### 3. 開始使用

**TestSysConf**

```java
public class TestSysConf extends AbstractSysConf {

    private static final TestSysConf INSTANCE = new TestSysConf();

    private TestSysConf() {
    }

    public static TestSysConf getInstance() {
        return INSTANCE;
    }

    @Override
    public String getName() {
        // 影響設定檔的名稱, test-defined.conf 將是此範例的設定檔名稱
        return "test";
    }
}
```

**SampleClass**

```java
public class SampleClass {
    
    public static void main(String[] args) {
        SysConf sysConf = TestSysConf.getInstance();
        
        // You will get your value if you ever set your configuration in test-defined.conf
        String testValue = sysConf.getValue("defined.test.value", "default-value");

        System.out.println("defined.test.value=" + testValue);
    }
}
```

**執行結果**

<pre>
<font color="#A00">Couldn't find resource: test-default.conf</font>
<font color="#A00">Couldn't find resource: test-defined.conf</font>
defined.test.value=default-value
</pre>

**設定 `test-defined.conf`**

1. 在執行目錄下新增 `conf` 資料夾
2. 建立 `test-defined.conf`
3. 新增 `defined.test.value=your-value`

**再次執行結果**

<pre>
<font color="#A00">Couldn't find resource: test-default.conf</font>
<font color="#0A0">Loading resource: test-defined.conf</font>
defined.test.value=your-value
</pre>

**其他**

1. 將預設的設定定義在 `test-default.conf` 中，並且打包在要執行的 Jar 檔，執行時將會自動載入
2. 設定在系統 System.getProperties() 中的參數會最優先被使用
3. 如果你的 `conf` 資料夾不在當前的目錄中，請設定 `-Dsys.conf={Your Path}/conf` JVM 參數直接指定
