package tw.cookie.tools.conf4j;

public class TestSysConf  extends AbstractSysConf {

    private static final TestSysConf INSTANCE = new TestSysConf();

    private TestSysConf() {
    }

    public static TestSysConf getInstance() {
        return INSTANCE;
    }

    @Override
    public String getName() {
        return "test";
    }
}
