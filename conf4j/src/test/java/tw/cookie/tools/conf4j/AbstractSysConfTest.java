package tw.cookie.tools.conf4j;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class AbstractSysConfTest {

    private SysConf sysConf = TestSysConf.getInstance();

    @BeforeClass
    public static void beforeClass() throws Exception {
        System.setProperty("system.test.value", "ok");
    }

    @Test
    public void getTestValue() throws Exception {
        assertThat("ok".equals(sysConf.getValue("default.test.value")), is(Boolean.TRUE));
        assertThat("ok".equals(sysConf.getValue("defined.test.value")), is(Boolean.TRUE));
        assertThat("ok".equals(sysConf.getValue("system.test.value")), is(Boolean.TRUE));
    }

    @Test
    public void getTestDefaultValue() throws Exception {
        assertThat("ok".equals(sysConf.getValue("default.not.exist.value", "ok")), is(Boolean.TRUE));
        assertThat("ok".equals(sysConf.getValue("defined.not.exist.value", "ok")), is(Boolean.TRUE));
        assertThat("ok".equals(sysConf.getValue("system.not.exist.value", "ok")), is(Boolean.TRUE));
    }

    @Test
    public void isTest() throws Exception {
        assertThat(sysConf.isTest(), is(Boolean.TRUE));
    }

    @Test
    public void notExist() throws Exception {
        new AbstractSysConf() {
            @Override
            public String getName() {
                return "not-exist";
            }
        };
    }

    @Test
    public void getName() throws Exception {
        assertThat("test".equals(sysConf.getName()), is(Boolean.TRUE));
    }
}