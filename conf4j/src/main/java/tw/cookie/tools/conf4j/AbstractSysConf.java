package tw.cookie.tools.conf4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public abstract class AbstractSysConf implements SysConf {
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractSysConf.class);

    private static final String DEFAULT_CONF_PATTERN = "%s-default.conf";
    private static final String DEFINED_CONF_PATTERN = "%s-defined.conf";

    private final Properties system = System.getProperties();
    private final Properties props;

    protected AbstractSysConf() {
        props = create();
    }

    private Properties create() {
        Properties properties = new Properties();

        load(properties, String.format(DEFAULT_CONF_PATTERN, getName()));
        load(properties, String.format(DEFINED_CONF_PATTERN, getName()));

        return properties;
    }

    private void load(Properties properties, String resource) {
        try (InputStream inputStream = ClassLoader.getSystemResourceAsStream(resource)) {
            properties.load(inputStream);
        } catch (Throwable t) {
            try (FileInputStream fileInputStream = new FileInputStream(new File(System.getProperty("sys.conf", "conf"), resource))) {
                properties.load(fileInputStream);
            } catch (IOException e) {
                LOGGER.warn(String.format("Couldn't find resource: %s", resource));
                return;
            }
        }
        LOGGER.info(String.format("Loading resource: %s", resource));
    }

    @Override
    public String getValue(String key) {
        return getProperty(key, null);
    }

    @Override
    public String getValue(String key, String defaultValue) {
        return getProperty(key, defaultValue);
    }

    private String getProperty(String key, String defaultValue) {
        String result = this.props.getProperty(key);
        return result == null? system.getProperty(key, defaultValue) : result;
    }

    @Override
    public boolean isTest() {
        return Boolean.valueOf(getValue("core.test", "false"));
    }
}
