package tw.cookie.tools.conf4j;

public interface SysConf {
    String getValue(String key);
    String getValue(String key, String defaultValue);
    String getName();
    boolean isTest();
}
